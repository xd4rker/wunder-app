## Development environment

- MySQL 5.7.19
- PHP 7.1.9

## Configuration

- Database configuration file is located under app/config/database.php.
- DocumentRoot should be pointed to "public" folder.

## Performance optimizations

- Store clients data using Cookies or LocalStorage instead of sessions.
- Perform API calls from within the browser using Javascript (client-side).

## Improvements

- Create a proper routing system.
- Dynamically map models to database tables / Use an Object-relational mapper (ORM).