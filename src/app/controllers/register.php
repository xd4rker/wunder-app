<?php

class Register extends Controller
{
	protected $request;
	protected $apiUrl;

	public function __construct() {
		$this->request = new Request;
		$this->apiUrl = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
	}

	public function index() {

		$sessionData = $this->request->session()->all();

		$paymentDataId = null;

		// Get the next step

		$nextStep = count($sessionData) + 1;

		// Check if all steps are completed
		
		if($nextStep == 4) {

			// Retrieve user info from session

			$userInfo = json_decode($sessionData['step_1'], true);
			$userAddress = json_decode($sessionData['step_2'], true);
			$userPaymentInfo = json_decode($sessionData['step_3'], true);

			// Save user information

			$user = $this->model('User');
			$user->create($userInfo);

			// Save user address

			$address = $this->model('Address');
			$address->create(array_merge($userAddress, ['user_id' => $user->id]));

			// Save user payment details

			$paymentInfo = $this->model('PaymentInfo');
			$paymentInfo->create(array_merge($userPaymentInfo, ['user_id' => $user->id]));

			// Send API request

			$payload = ['customerId' => $user->id, 'iban' => $paymentInfo->iban, 'owner' => $paymentInfo->owner];

			$resp = Request::create($this->apiUrl, json_encode($payload), ['Content-Type: application/json']);

			if($resp['status'] == 200) {
				$paymentData = json_decode($resp['body'], true);
				
				if($paymentData === null || !isset($paymentData['paymentDataId'])) {
					Request::abort(500, 'Unexpected response');
				}

				// Save user payment data id

				$payment = $this->model('Payment');
				$payment->create(['payment_info_id' => $paymentInfo->id, 'payment_data_id' => $paymentData['paymentDataId']]);

				$paymentDataId = $paymentData['paymentDataId'];

				// Destroy session

				$this->request->session()->destroy();

			} else {
				Request::abort(500, 'Error');
			}

		}

		$this->view('register/index', ['nextStep' => $nextStep, 'paymentDataId' => $paymentDataId]);
	}

	public function post() {

		if($this->request->has('step')) {
			
			// Store current step's info into session

			$this->request->session()->put('step_' . $this->request->input('step'), json_encode($this->request->all()));
			Request::redirect('/register');
		}
	}
}