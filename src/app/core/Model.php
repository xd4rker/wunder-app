<?php

class Model
{
	protected $db;

	public function __construct() {
		$this->db = new Database;
	}

	public function create($modelData) {

		// Get model properties

		$reflect = new ReflectionObject($this);
		$propsObj = $reflect->getProperties(ReflectionProperty::IS_PUBLIC);

		$properties = array_map(function($item) {
			return $item->getName();
		}, $propsObj);

		// Filter model properties from $modelData + Build insert query

		$queryCols = [];
		$bindParams = [];

		foreach (array_keys($modelData) as $key) {
			if(!in_array($key, $properties)) {
				unset($modelData[$key]);
			} else {
				$queryCols[] = $key;
				$bindParams[] = ':' . $key;
				$this->{$key} = $modelData[$key];
			}
		}

		$query = 'INSERT INTO ' . $this->table . ' (' . implode(', ', $queryCols) . ') VALUES (' . implode(', ', $bindParams) . ')';

		// Insert model data
		$this->id = $this->db->query($query, $modelData, Database::FETCH_ID);
	}

}