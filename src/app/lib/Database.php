<?php

class Database {

	protected $db;

	const FETCH_ALL = 1;
	const FETCH_ID = 2;

	public function __construct() {
		$this->db = $this->connect();
	}

	public function __destruct() {
		$this->db = null;
	}
	
	private function connect() {
		try {
			$db = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
		} catch (Exception $e) {
			die('<pre>Error Establishing Connection : ' . $e->getMessage() . '</pre>');
		}

		return $db;
	}

	public function query($query, $args = [], $flag = null) {
		$stm = $this->db->prepare($query);
		$stm->execute($args);

		switch ($flag) {
			case self::FETCH_ALL:
				return $stm->fetchAll();
				break;
			case self::FETCH_ID:
				return $this->db->lastInsertId();
				break;
		}
	}
}