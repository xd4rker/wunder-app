<?php

Class Request {

	protected $session;

	public function __construct() {
		$this->session = new Session;
	}

	public function getRequestVar($key, $method) {
		return isset($method[$key]) ? htmlentities((string) $method[$key]) : null;
	}

	public function input($key) {
		return $this->getRequestVar($key, $_POST);
	}

	public function query($key) {
		return getRequestVar($key, $_GET);
	}

	public function all() {
		return array_map(function($item) {
			return htmlentities((string) $item);
		}, $_POST);
	}

	public function has($key) {
		return isset($_POST[$key]);
	}

	public function session() {
		return $this->session;
	}

	public static function redirect($path) {
		header('Location: ' . $path);
		die();
	}

	public static function abort($status, $message = null) {
		http_response_code($status);
		die('<pre>' . $message . '</pre>');
	}

	public static function create($url, $data = null, $headers = []) {

		$ch = curl_init($url);
		
		if($data) {
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// For the sake of demonstration, I have set this to false
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$result = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		return ['body' => $result, 'status' => $httpcode];
	}
}