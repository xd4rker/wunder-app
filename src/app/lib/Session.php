<?php

class Session {

	public function __construct() {
		if (session_status() == PHP_SESSION_NONE) {
			// set session lifetime
			session_set_cookie_params(3600*24*365);
		    session_start();
		}
	}

	public function has($key) {
		return isset($_SESSION[$key]);
	}

	public function put($key, $value) {
		$_SESSION[$key] = $value;
	}

	public function get($key) {
		return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
	}

	public function remove($key) {
		unset($_SESSION[$key]);
	}

	public function flush() {
		$_SESSION = [];
	}

	public function all() {
		return $_SESSION;
	}

	public static function destroy() {
		session_destroy();
	}
}