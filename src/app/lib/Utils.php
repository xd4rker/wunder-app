<?php

Class Utils {

	public static function render($page, $vars = []) {
		$data = $vars;
		require_once(dirname(__FILE__)."/../views/".$page.".php");
	}
}