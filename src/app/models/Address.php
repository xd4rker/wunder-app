<?php

class Address extends Model
{
	protected $table;

	public $id;
	public $user_id;
	public $address;
	public $zipcode;
	public $city;

	public function __construct() {
		parent::__construct();
		$this->table = 'addresses';
	}
}