<?php

class Payment extends Model
{
	protected $table;

	public $id;
	public $payment_info_id;
	public $payment_data_id;
	public $created_at;

	public function __construct() {
		parent::__construct();
		$this->table = 'payments';
	}
}