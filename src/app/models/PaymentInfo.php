<?php

class PaymentInfo extends Model
{
	protected $table;

	public $id;
	public $user_id;
	public $owner;
	public $iban;

	public function __construct() {
		parent::__construct();
		$this->table = 'payment_info';
	}
}