<?php

class User extends Model
{
	protected $table;

	public $id;
	public $firstname;
	public $lastname;
	public $telephone;
	public $created_at;

	public function __construct() {
		parent::__construct();
		$this->table = 'users';
	}
}