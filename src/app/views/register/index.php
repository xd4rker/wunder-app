<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Wunder</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.1.1/lumen/bootstrap.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"></script>

    <style>
      body {
        padding-top: 54px;
      }
      
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
      
      .highlighted {
      	background-color: #f3f3f3;
      	padding: 20px;
      	border: 1px solid #e9e9e9;
      }

      .active {
      	background-color: #e3e3e3;
      	border-bottom: 2px solid #c8c8c8;
      }
  	</style>
  </head>

  <body>

  <?= Utils::render('layouts/navbar'); ?>

    <div class="container mt-5">

    	<div class="row justify-content-center">
    		<div class="col-md-2 highlighted <?= ($data['nextStep'] == 1) ? 'active' : '' ?>">
    			<i class="fas fa-user"></i>&nbsp; Personal information
    		</div>
    		<div class="col-md-2 highlighted <?= ($data['nextStep'] == 2) ? 'active' : '' ?>">
    			<i class="fas fa-map-marker-alt"></i>&nbsp; Address information
    		</div>
    		<div class="col-md-2 highlighted <?= ($data['nextStep'] == 3) ? 'active' : '' ?>">
    			<i class="fas fa-credit-card"></i>&nbsp; Payment information
    		</div>
    		<div class="col-md-2 highlighted <?= ($data['nextStep'] == 4) ? 'active' : '' ?>">
    			<i class="fas fa-check"></i>&nbsp; Sucess
    		</div>
    	</div>

      <!-- Personal information -->

      <?php if($data['nextStep'] == 1): ?>
      <div class="row justify-content-center">
        <div class="col-md-8 highlighted">
          <form action="/register/post" method="post">
            <input type="hidden" name="step" value="1">
            <div class="form-group">
              <label for="exampleInputEmail1">First Name</label>
              <input type="text" class="form-control" name="firstname" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Last Name</label>
              <input type="text" class="form-control" name="lastname" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Telephone</label>
              <input type="tel" class="form-control" name="telephone" required>
            </div>
            <button type="submit" class="btn btn-primary">Next</button>
          </form>
        </div>
      </div>
      <?php endif; ?>

      <!-- Address information -->
      
      <?php if($data['nextStep'] == 2): ?>

      <div class="row justify-content-center">
        <div class="col-md-8 highlighted">
          <form action="/register/post" method="post">
            <input type="hidden" name="step" value="2">
            <div class="form-group">
              <label>Address</label>
              <input type="text" class="form-control" name="address" placeholder="Street, house number" required>
            </div>
            <div class="form-row">
              <div class="form-group col-md-2">
                <label>Zip</label>
                <input type="text" class="form-control" name="zipcode" required>
              </div>
              <div class="form-group col-md-6">
                <label>City</label>
                <input type="text" class="form-control" name="city" required>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Next</button>
          </form>
        </div>
      </div>
      <?php endif; ?>

      <!-- Payment Information -->

      <?php if($data['nextStep'] == 3): ?>

      <div class="row justify-content-center">
        <div class="col-md-8 highlighted">
          <form action="/register/post" method="post">
            <input type="hidden" name="step" value="3">
            <div class="form-group">
              <label>Account Owner</label>
              <input type="text" class="form-control" name="owner">
            </div>
            <div class="form-group">
              <label>IBAN</label>
              <input type="text" class="form-control" name="iban">
            </div>
            <button type="submit" class="btn btn-primary">Next</button>
          </form>
        </div>
      </div>
      <?php endif; ?>

      <!-- Success -->

      <?php if($data['nextStep'] == 4): ?>

      <div class="row justify-content-center">
        <div class="col-md-8 highlighted">
          <p>Payment successfully completed.</p>
          <hr>
          <h5>Payment ID</h5>
          <code><?= $data['paymentDataId'] ?></code>
        </div>
      </div>
      <?php endif; ?>

    </div>

  </body>
</html>